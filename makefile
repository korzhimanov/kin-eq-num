.RECIPEPREFIX = >

html: notebooks/$(N).ipynb
> jupyter nbconvert notebooks/$(N).ipynb --to html --TemplateExporter.exclude_input=True --output ../html/$(N)

pdf: notebooks/$(N).ipynb
> pandoc -V papersize=A4 -V fontenc=T2A -V linkcolor=cyan -V babel-lang=russian -V mainfont="Liberation Serif" --pdf-engine=xelatex -f ipynb notebooks/$(N).ipynb -o pdf/$(N).pdf

slides: notebooks/$(N).ipynb
> jupyter nbconvert notebooks/$(N).ipynb --to slides --TemplateExporter.exclude_input=True --TemplateExporter.extra_template_basedirs=./templates --template=reveal-modified --output ../slides/$(N)