# Переменные

x = 1

ω = 2.0

температура = 13

y = 2*x

y = 3x

y = ωx

# Нельзя:
# - начинать имя с числа
# - использовать некоторые спецсимволы
# - использовать зарезервированные системные имена

1x = 1

x@y = 2

struct = 3.0

# Базовые типы

typeof(3)

typeof(3.0)

typeof("Hello")

typeof("s")

typeof('s')

x = 3
typeof(x)

y = Float32(x)

typeof(y)

y = convert(Float64, x)

typeof(y)

typeof(true)

typeof(false)

# Tuple (кортеж) - неизменяемая упорядоченная последовательность
x = (1, 2, 4)

typeof(x)

# Индексирование начинается с 1, как в Matlab, а не в Си (!)
x[1]

x[1] = 1

# Dictionary (словарь или ассоциативный массив) - изменяемая неупорядоченная последовательность
d = Dict("a" => 1, "b" => 2)

d["a"]

d["a"] = 3

d["a"]

d["c"] = 4

print(d)

# Array (массив) - изменяемая последовательность
a = Array{Float64}(undef, 2, 3, 4)

typeof(a)

typeof([1, 2, 3])

typeof([1 2; 3 4])

# Работа с массивами

a = [0.1, 0.2, 0.3, 0.4]

a[1]

a[1+2]

a[end]

a[end-1]

print(a[1:3])

print(a[1:2:3])

print(a[[1,2,4]])

b = reshape(a, 2, 2)

# Массивы итерируются по индексам, начиная с первого, как в Fortran, а не в Си! (column-first)
print(b)

print(b[:, 1])

print(b[1, :])

# Массивы по умолчанию передаются по ссылке, а не по значению
b[1,1] = 0.5

a[1]

c = copy(a)

c[1] = 0.6

a[1]

# Можно создавать разные "виды" (view) на один и тот же массив
d = view(a, 1:2:4)

print(a, d)

typeof(d)

d[2] = 0.7

print(a)

# Создание массивов

f = zeros(3, 4)

f = ones(3, 4)

g = range(0.0, stop=2.0, length=10)

print(g)

typeof(g)

h = collect(g)

print(h)

# В более сложных случаях удобно использовать списковые включения (comprehensions)
k = [x*y for x = 1:4, y = 1:3]

# Полезные функции для работы с массивами
length(k)

size(k)

ndims(k)

sum(k)

permutedims(k)

k

# Векторно-матричные и поэлементные операции

a = [x for x = 1:3]

b = [x^2 for x = 1:3]

c = [x+y for x = 1:3, y = 1:4]

print(a+b)

print(a*b)

print(a.*b)

print(a*b')

print(a'*b)

print(c)
print(a+c)
print(a.+c)

# Это называется broadcasting

print(a*c)
print(a'*c)

print(a\c)
print(c/a)
print(c'/a')

# Программные конструкции
a = begin
    b = 2
    3 * b
end

if a < 4
    b = 1
elseif a > 7
    b = 2
else
    b = 3
end

if a < 4 || a > 5
    b = 1
else
    b = 2
end

i = 1
k = 1
while true
    global i += 1
    if i < 10
        continue
    else
        break
    end
    global k = 2
end

i, k

for x = 1:10
    print(x, " ")
end

for x in 1:10
    print(x, " ")
end

d = Dict("a" => 1, "b" => 2, "c" => 3)
for x in d
    print(x, " ")
end

for x in keys(d)
    println(x, " => ", d[x])
end

for x in values(d)
    print(x, " ")
end

a = [0.1*x for x = 1:6]

for x in a
    print(x, " ")
end

for x in eachindex(a)
    println(x, " : ", a[x])
end

b = collect(reshape(a, 2, 3))

for x in b
    print(x, " ")
end

for x in eachindex(b)
    println(x, " : ", b[x])
end

for x in CartesianIndices(b)
    println(x, " : ", b[x])
end

# Функции
function u(x, y)
    return x+y
end

u(1,2)

u(x,y) = x*y

u(2,3)

function u(x, y=5)
    return x+y
end

u(1)

u(1,2)

# Именованные параметры
function t(x; y=4)
    return x+y
end

t(1)

t(1,2)

t(1, y=2)

# Функции можно передавать другим функциям и возвращать из функций
function z(a)
    ret(x) = a(x)
    return ret
end

r = z(sin)

r(0)

r(π)

r(π/2)

s = z(cos)

s(0)

s(π)

y = map(sin, [π*x/10 for x = 0:10])

print(y)

# Анонимные функции
x -> x^2

(x, y) -> x^y

(x -> x^2)(2)

y = map((x) -> sin(x^2), [π*x/10 for x = 0:10])

# Функция может иметь много методов
u(x, y) = x / y
u(x::Integer, y::Integer) = 0

u(1.0, 2.0)

u(1, 2.0)

u(1, 2)

methods(u)

# Видимость переменных
# Различают глобальную и локальную области видимости переменных
# Локальную область видимости, в частности, создают for, while, function, comprehensions
# Не создают - begin-end и if

a = 1

if true
    a = 2
end

a

for x = 1:3
    a = x
end

a

for x = 1:3
    global a = x
end

a

function u()
    a = 1
    for b = 1:3
        a = b
    end
    return a
end

u()

function u()
    a = 1
    for b = 1:3
        local a = b
    end
    return a
end

u()

function u()
    b = 1
    for b = 1:3
        a = b
    end
    return b
end

u()

function u()
    b = 1
    for outer b = 1:3
        a = b
    end
    return b
end

u()
