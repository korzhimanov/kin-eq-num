# kin-eq-num

Материалы учебного курса по численному моделированию кинетических уравнений для плазмы

1. Кинетическое описание плазмы [pdf](pdf/1.pdf) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/korzhimanov%2Fkin-eq-num/master?filepath=notebooks%2F1.ipynb)
2. Введение в методы моделирования кинетического уравнения [pdf](pdf/2.pdf)[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/korzhimanov%2Fkin-eq-num/master?filepath=notebooks%2F2.ipynb)
3. Универсальные методы интегрирования одномерного уравнения переноса [pdf](pdf/3.pdf) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/korzhimanov%2Fkin-eq-num/master?filepath=notebooks%2F3.ipynb)
4. Метод конечных объёмов и полулагранжев метод [pdf](pdf/4.pdf) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/korzhimanov%2Fkin-eq-num/master?filepath=notebooks%2F4.ipynb)